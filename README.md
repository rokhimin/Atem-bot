<img align="center" width="70%" src="https://i.imgur.com/rvGL5jK.gif" />

<img align="center" width="150" src="https://i.imgur.com/Fgolqn1.png" />

<a href="https://discordbots.org/bot/617492380710469763" >
  <img src="https://discordbots.org/api/widget/status/617492380710469763.svg" alt="Atem" />
</a>

![Lang](https://img.shields.io/badge/language-ruby-red)
[![Build Status](https://travis-ci.com/rokhimin/Atem-bot.svg?branch=master)](https://travis-ci.com/rokhimin/Atem-bot)

# Atem
discord & telegram bot for search yugioh card, newst meta deck . written in Ruby.

### todo
- ~~Line~~ 

### play

> Discord : https://discordbots.org/bot/617492380710469763

> Telegram : https://t.me/Atem_YugiohBot

### features
- quick search yugioh card 
- list search yugioh card
- tier list deck meta (duel links)

### usage
|   Commands    |    Discord    |    Telegram    |
| ------------- | ------------- | ------------- |
| information  | ```atem:info``` | ```/info``` |
| ping | ```atem:ping``` |  |
| meta deck (duel links) | ```atem:dlmeta```  | ```/duellinksmeta``` |
| random card | ```atem:random``` | ```/random``` |
| list search card  |  ```atem:src card_name```    | ```/searchlist``` |
| quick search card | ```::card_name::``` | ```::card_name::``` |

### developer tool tasks
install gem :
 ```
 rake gem:install
 ```
create db :
 ```
 rake db:create
 ```
migration db :
 ```
 rake db:migrate
 ```
drop db :
 ```
 rake db:drop
 ```
test :
 ```
 rake run:rspec
 ```
run bot discord :
 ```
 rake run:discord
 ```
run bot telegram :
 ```
 rake run:telegram
 ```


# License
Apache License.


<a href="https://discordbots.org/bot/617492380710469763" >
  <img src="https://discordbots.org/api/widget/617492380710469763.svg" alt="Atem" />
</a>
